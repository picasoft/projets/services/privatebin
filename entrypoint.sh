#! /bin/sh

cp /conf.php /srv/cfg/conf.php
mkdir /tmp/metrics
cp /metrics.php /tmp/metrics/metrics.php

php << EOF
<?php
\$configPath = "/var/lib/unit/conf.json";

\$oldJsonString = file_get_contents(\$configPath);
\$customRouteJsonString = "{\"match\":{\"uri\":\"/metrics.php\"},\"action\":{\"pass\":\"applications/metrics\"}}";
\$customAppJsonString = "{\"type\":\"php\",\"root\":\"/tmp/metrics/\",\"script\":\"metrics.php\"}";

\$oldJson = json_decode(\$oldJsonString, true);
\$customRouteJson = json_decode(\$customRouteJsonString, true);
\$customAppJson = json_decode(\$customAppJsonString, true);

array_unshift(\$oldJson["routes"], \$customRouteJson);
\$oldJson["applications"]["metrics"] = \$customAppJson;

\$newJsonString = json_encode(\$oldJson, JSON_UNESCAPED_SLASHES);

file_put_contents(\$configPath, \$newJsonString);
EOF

sed -i "s/usr = \"\"/usr = \"$PB_USER\"/" /srv/cfg/conf.php
sed -i "s/pwd = \"\"/pwd = \"$PB_PASSWD\"/" /srv/cfg/conf.php
sed -i "s/dsn = \"\"/dsn = \"pgsql:host=$PB_HOST;port=$PB_PORT;dbname=$PB_NAME\"/" /srv/cfg/conf.php
sed -i "s/PB_HOST/$PB_HOST/; s/PB_PORT/$PB_PORT/; s/PB_NAME/$PB_NAME/; s/PB_USER/$PB_USER/; s/PB_PASSWD/$PB_PASSWD/; s/PB_AUTH_USER/$PB_AUTH_USER/; s/PB_AUTH_PASSWD/$PB_AUTH_PASSWD/" /tmp/metrics/metrics.php

/usr/sbin/unitd --no-daemon --log /dev/stdout --tmpdir /tmp
