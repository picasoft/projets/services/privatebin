;<?php http_response_code(403);
[main]
name = "PicaPaste"

basepath = "https://paste.picasoft.net/"

discussion = true

opendiscussion = false

password = true

fileupload = false

burnafterreadingselected = false

defaultformatter = "plaintext"

sizelimit = 10485760

template = "bootstrap"

notice = ""

info = "Consulter nos <a href='https://picasoft.net/co/cgu.html'>CGU</a>"

languageselection = false

languagedefault = "fr"

qrcode = true

email = true

icon = "none"

cspheader = "default-src 'none'; manifest-src 'self'; connect-src * blob:; script-src 'self' 'unsafe-eval'; style-src 'self'; font-src 'self'; img-src 'self' * data: blob:; media-src blob:; object-src blob:; sandbox allow-same-origin allow-scripts allow-forms allow-popups allow-modals"

zerobincompatibility = false

httpwarning = true

compression = "zlib"

[expire]
default = "1week"

[expire_options]
5min = 300
10min = 600
1hour = 3600
1day = 86400
1week = 604800
1month = 2592000
1year = 31536000

[formatter_options]
plaintext = "Plain Text"
syntaxhighlighting = "Source Code"
markdown = "Markdown"

[traffic]
limit = 10
header = "X_FORWARDED_FOR"

[purge]
limit = 300
batchsize = 40


[model]

class = Database

[model_options]
dsn = ""
tbl = ""
usr = ""
pwd = ""
opt[12] = true  ; PDO::ATTR_PERSISTENT
